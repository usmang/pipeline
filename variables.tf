
variable "Network" {
  type = string
}
variable "date" {
  type = string
}

variable "account_id_iam" {
  type = number
}

variable "bucket_arn" {
  type = string
}

variable "group" {
  type= string
}


variable "rds_config" {
  type = object({
    rds_instance_class      = string
    rds_allocated_storage   = number
    rds_username            = string #used in rds indentifer like rds or rds-1
    rds_major_version       = string
    backup_retention_period = number
    backup_window           = string
    maintenance_window      = string
    multi_az                = bool
    rds_engine              = string
    engine_version          = string
    rds_version             = string
    sql_server_version      = string
    max_allocated_storage   = number
  })
}


variable "secret_config" {
  type = object({
    admin_user    = string
    sql_engine   = string
    dbport        = number
    database_name = string
    dbuser        = string
    sa_user       = string
    db_version    = string
    secret_env    = string
  })
}

variable "tg_config" { 
  type = object({
    health_interval         = number
    health_check_path       = string
    app_server_target_port  = number
    health_check_protocol   = string
    healthy_threshold       = number
    unhealthy_threshold     = number
    health_check_timeout    = number
    tg_deregistration_delay = number
    target_type             = string
    app_server_protocol     = string
    app_server_target_port  = number
  })
}

variable "lb_config" {
  type = object({
    stickiness_type   = string
    cookie_duration   = number
    ssl_policy        = string
    listener_port     = number
    listener_protocol = string
    lb_type           = string
  })
}


variable "sqs_config" {
  type = object({
    delay_seconds = number
    max_msg_size  = number
    msg_retention = number
    rcv_time_wait = number
    sqs_version   = string
  })
}

variable "sql_config" {
  type = object({
    iops                       = number
    sql_server_ebs_volume_size = number
    sql_server_ami_id          = string
    volume_type                = string
    sql_key                    = string
    ebs_az                     = string
    sql_server_instance_type   = string
  })
}



#tags
variable "tags" {
  type = object({
    environment = string
    owner       = string
    AWS_REGION  = string
    ticket      = string
  })
}


variable "sql_server_ingress_rules" {
  type = map(object({
    type        = string
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = optional(list(string))
    source      = optional(string)
    description = string
  }))
}

variable "sql_server_egress_rules" {
  type = map(object({
    type        = string
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = optional(list(string))
  }))
  default = {
    rule1 = {
      type        = "egress"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
}


#LOAD BALANCR and Target Group
variable "app_lb_ingress_rules" {
  type = map(object({
    type             = string
    from_port        = number
    to_port          = number
    protocol         = string
    cidr_blocks      = optional(list(string))
    ipv6_cidr_blocks = optional(list(string))
    source           = optional(string)
  }))
}

variable "app_lb_egress_rules" {
  type = map(object({
    type             = string
    from_port        = number
    to_port          = number
    protocol         = string
    cidr_blocks      = optional(list(string))
    ipv6_cidr_blocks = optional(list(string))
  }))
}









variable "sql_db_access_sg_ingress" {
  type = map(object({
    type        = string
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = optional(list(string))
    source      = optional(string)
    description = string
  }))
}

variable "sql_db_access_sg_egress" {
  type = map(object({
    type        = string
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = optional(list(string))
  }))
}


variable "admin_sg_ingress" {
  type = map(object({
    type             = string
    from_port        = number
    to_port          = number
    protocol         = string
    cidr_blocks      = optional(list(string))
    ipv6_cidr_blocks = optional(list(string))
    source           = optional(string)
    description      = string
  }))
}

variable "admin_sg_egress" {
  type = map(object({
    type             = string
    from_port        = number
    to_port          = number
    protocol         = string
    cidr_blocks      = optional(list(string))
    ipv6_cidr_blocks = optional(list(string))
  }))

}


variable "app_sg_ingress" {
  type = map(object({
    type             = string
    from_port        = number
    to_port          = number
    protocol         = string
    cidr_blocks      = optional(list(string))
    ipv6_cidr_blocks = optional(list(string))
    source           = optional(string)
    description      = string
  }))

}

variable "app_sg_egress" {
  type = map(object({
    type             = string
    from_port        = number
    to_port          = number
    protocol         = string
    cidr_blocks      = optional(list(string))
    ipv6_cidr_blocks = optional(list(string))
  }))

}


variable "rds_security_group_ingress" {
  type = map(object({
    type        = string
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = optional(list(string))
    source      = optional(string)
    description = string
  }))
}

variable "rds_security_group_egress" {
  type = map(object({
    type        = string
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = optional(list(string))
  }))
}



variable "rds_sg_db_ingress" {
  type = map(object({
    type        = string
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = optional(list(string))
    source      = optional(string)
    description = string
  }))
}

variable "rds_sg_db_egress" {
  type = map(object({
    type        = string
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = optional(list(string))
  }))
}