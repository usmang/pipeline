terraform {
  required_version = "= 1.4.6"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.10.0"
    }
    random = {
      version = "3.5.1"
    }
  }
}

provider "aws" {
  profile = "tcp-qa"
  alias   = "prod01"
  region  = "us-east-1"
  default_tags {
    tags = {
      Environment = var.tags.environment
      Owner       = var.tags.owner
      Project     = var.group
      Region      = var.tags.AWS_REGION
      Team        = "CloudOps"
      Terraform   = "true"
      Ticket      = var.tags.ticket
    }
  }

}

provider "aws" {
  profile = "dmiinfrastructure01"
  alias   = "dmi-infra"
  region  = "us-east-1"
  default_tags {
    tags = {
      Environment = var.tags.environment
      Owner       = var.tags.owner
      Project     = var.group
      Region      = var.tags.AWS_REGION
      Team        = "CloudOps"
      Terraform   = "true"
      Ticket      = var.tags.ticket
    }
  }

}


provider "aws" {
  profile = "tcp-qa"
  region  = "us-east-1"
}

