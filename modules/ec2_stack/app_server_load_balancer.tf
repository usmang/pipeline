# Create Load Balancer
resource "aws_lb" "app_lb" {
  provider           = aws.prod01
  name               = "${var.group}-ALB"
  load_balancer_type = var.lb_config.lb_type
  internal           = false
  subnets            = [for subnet in data.aws_subnet.public_selected : subnet.id] # Updated line
  security_groups    = [aws_security_group.app_lb_sg.id]
}

# Create Listener
resource "aws_lb_listener" "app_lb_listener" {
  provider          = aws.prod01
  load_balancer_arn = aws_lb.app_lb.arn
  port              = var.lb_config.listener_port
  protocol          = var.lb_config.listener_protocol
  certificate_arn   = data.aws_acm_certificate.lb_certificate.arn
  ssl_policy        = var.lb_config.ssl_policy

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.app_target_group.arn
  }
}
# Create Target Group
resource "aws_lb_target_group" "app_target_group" {
  provider             = aws.prod01
  name                 = "${var.group}-ALB-Port-${var.app_server_target_port}-TG"
  port                 = var.tg_config.app_server_target_port
  protocol             = var.tg_config.app_server_protocol
  vpc_id               = data.aws_vpc.selected.id
  target_type          = var.tg_config.target_type
  deregistration_delay = var.tg_config.tg_deregistration_delay


  stickiness {
    type            = var.lb_config.stickiness_type
    cookie_duration = var.lb_config.cookie_duration
  }

  health_check {
    interval            = var.tg_config.health_interval
    path                = var.tg_config.health_check_path
    port                = var.tg_config.app_server_target_port
    protocol            = var.tg_config.health_check_protocol
    healthy_threshold   = var.tg_config.healthy_threshold
    unhealthy_threshold = var.tg_config.unhealthy_threshold
    timeout             = var.tg_config.health_check_timeout
  }
}
