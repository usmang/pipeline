variable "tg_config" { # variables for target group and health check
  type = object({
    health_interval         = number
    health_check_path       = string
    app_server_target_port  = number
    health_check_protocol   = string
    healthy_threshold       = number
    unhealthy_threshold     = number
    health_check_timeout    = number
    tg_deregistration_delay = number
    target_type             = string
    app_server_protocol     = string
    app_server_target_port  = number
  })
}

variable "lb_config" {
  type = object({
    stickiness_type   = string
    cookie_duration   = number
    ssl_policy        = string
    listener_port     = number
    listener_protocol = string
    lb_type           = string
  })
}

variable "Network" {
  type = string
}

variable "group"{
  type= string
}
variable "sqs_config" {
  type = object({
    delay_seconds = number
    max_msg_size  = number
    msg_retention = number
    rcv_time_wait = number
    secret_env    = string
    sqs_version   = string
  })
}

variable "tags" {
  type = object({
    environment = string
    owner       = string
    AWS_REGION  = string
    ticket      = string
  })
}

variable "app_lb_ingress_rules" {
  type = map(object({
    type             = string
    from_port        = number
    to_port          = number
    protocol         = string
    cidr_blocks      = optional(list(string))
    ipv6_cidr_blocks = optional(list(string))
    source           = optional(string)
  }))
}

variable "app_lb_egress_rules" {
  type = map(object({
    type             = string
    from_port        = number
    to_port          = number
    protocol         = string
    cidr_blocks      = optional(list(string))
    ipv6_cidr_blocks = optional(list(string))
  }))
}