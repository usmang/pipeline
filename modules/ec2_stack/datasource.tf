data "aws_vpc" "selected" {
  filter {
    name   = "tag:Environment"
    values = ["${var.environment}"]
  }
}

data "aws_subnets" "private_selected" {
  filter {
    name   = "tag:Account"
    values = ["tcp-qa"]
  }
}

data "aws_subnets" "public_selected" {
  filter {
    name   = "tag:Network"
    values = ["Public"]
  }
}

data "aws_subnet" "private_selected" {
  for_each = toset(data.aws_subnets.private_selected.ids)
  id       = each.value
}

data "aws_subnet" "public_selected" {
  for_each = toset(data.aws_subnets.public_selected.ids)
  id       = each.value
}


data "aws_acm_certificate" "lb_certificate" {
  domain = "tcpsoftware.net"
  tags = {
    Name = "tcpsoftware.net"
  }
}

data "aws_sqs_queue" "dead_letter_queue" {
  provider = aws.dmi-infra
  name     = "provision-dead-letter-queue" # Replace with the name of your SQS queue
}


