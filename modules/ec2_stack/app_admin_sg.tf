/*
resource "aws_security_group" "admin_server_sg" {
  name        = "${var.group}-Admin-Server-SG"
  description = "Security Group for ${var.group} admin server"
  vpc_id      = data.aws_vpc.selected.id
  tags = {
    Name = "${var.group}-Admin-Server-SG"
  }
}


resource "aws_security_group_rule" "admin_sg_ingress" {
  for_each = var.admin_sg_ingress

  type                     = each.value.type
  from_port                = each.value.from_port
  to_port                  = each.value.to_port
  protocol                 = each.value.protocol
  security_group_id        = aws_security_group.admin_server_sg.id
  cidr_blocks              = each.value.cidr_blocks
  source_security_group_id = each.value.source != null ? each.value.source : null
  ipv6_cidr_blocks         = each.value.ipv6_cidr_blocks
  description              = each.value.description
}

 resource "aws_security_group_rule" "Allow_DB_Access_Rule"{
    type        = "ingress"
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    security_group_id = aws_security_group.admin_server_sg.id
    source_security_group_id    = module.tcp_rds.security_group_db_acess_id
    description = "Allow DB Access"
  }


resource "aws_security_group_rule" "admin_sg_egress" {
  for_each = var.admin_sg_egress

  type              = each.value.type
  from_port         = each.value.from_port
  to_port           = each.value.to_port
  protocol          = each.value.protocol
  security_group_id = aws_security_group.admin_server_sg.id
  cidr_blocks       = each.value.cidr_blocks
  ipv6_cidr_blocks  = each.value.ipv6_cidr_blocks
}



resource "aws_security_group" "app_server_sg" {
  name        = "${var.group}-App-Server-SG"
  description = "Security Group for  ${var.group} app server"
  vpc_id      = data.aws_vpc.selected.id
  tags = {
    Name = "${var.group}-App-Server-SG"
  }
}


resource "aws_security_group_rule" "app_sg_ingress" {
  for_each = var.app_sg_ingress

  type                     = each.value.type
  from_port                = each.value.from_port
  to_port                  = each.value.to_port
  protocol                 = each.value.protocol
  security_group_id        = aws_security_group.app_server_sg.id
  cidr_blocks              = each.value.cidr_blocks
  source_security_group_id = each.value.source != null ? each.value.source : null
  ipv6_cidr_blocks         = each.value.ipv6_cidr_blocks
  description              = each.value.description
}

 resource "aws_security_group_rule" "DB_Access_Rule"{
    type        = "ingress"
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    security_group_id =  aws_security_group.app_server_sg.id
    source_security_group_id  = module.tcp_rds.security_group_db_acess_id
    description = "Allow DB Access"
  }

 resource "aws_security_group_rule" "Allow_7012_Rule"{
    type        = "ingress"
    from_port   = 7012
    to_port     = 7012
    protocol    = "tcp"
    security_group_id =  aws_security_group.app_server_sg.id
    source_security_group_id     = aws_security_group.app_lb_sg.id
    description = "7012 from ALB ${var.group}"
  }

   resource "aws_security_group_rule" "Allow_7011_Rule"{
    type        = "ingress"
    from_port   = 7011
    to_port     = 7011
    protocol    = "tcp"
    security_group_id =  aws_security_group.app_server_sg.id
    source_security_group_id   = aws_security_group.app_lb_sg.id
    description = "${var.group}-ALB-sg"
  }

 resource "aws_security_group_rule" "Allow_80_Rule"{
    type        = "ingress"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    security_group_id =  aws_security_group.app_server_sg.id
    source_security_group_id   = aws_security_group.app_lb_sg.id
    description = "${var.group}-ALB-sg"
  }


resource "aws_security_group_rule" "app_sg_egress" {
  for_each = var.app_sg_egress

  type              = each.value.type
  from_port         = each.value.from_port
  to_port           = each.value.to_port
  protocol          = each.value.protocol
  security_group_id = aws_security_group.app_server_sg.id
  cidr_blocks       = each.value.cidr_blocks
  ipv6_cidr_blocks  = each.value.ipv6_cidr_blocks
}
*/