
resource "aws_security_group" "app_lb_sg" {
  provider    = aws.prod01
  name        = "${var.group}-ALB-sg"
  description = "Security Group for app load balancer"
  vpc_id      = data.aws_vpc.selected.id
  tags = {
    Name = "${var.group}-ALB-sg"
  }
}

resource "aws_security_group_rule" "app_lb_sg_ingress" {
  provider = aws.prod01
  for_each = var.app_lb_ingress_rules

  type                     = each.value.type
  from_port                = each.value.from_port
  to_port                  = each.value.to_port
  protocol                 = each.value.protocol
  security_group_id        = aws_security_group.app_lb_sg.id
  cidr_blocks              = each.value.cidr_blocks
  source_security_group_id = each.value.source != null ? each.value.source : null
  ipv6_cidr_blocks         = each.value.ipv6_cidr_blocks
}

resource "aws_security_group_rule" "app_lb_sg_egress" {
  provider = aws.prod01
  for_each = var.app_lb_egress_rules

  type              = each.value.type
  from_port         = each.value.from_port
  to_port           = each.value.to_port
  protocol          = each.value.protocol
  security_group_id = aws_security_group.app_lb_sg.id
  cidr_blocks       = each.value.cidr_blocks
  ipv6_cidr_blocks  = each.value.ipv6_cidr_blocks
}
