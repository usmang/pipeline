resource "aws_sqs_queue" "sqs_queue" {
  provider                          = aws.dmi-infra
  name                              = "${var.sqs_config.sqs_version}-${var.group}"
  delay_seconds                     = var.sqs_config.delay_seconds
  max_message_size                  = var.sqs_config.max_msg_size
  message_retention_seconds         = var.sqs_config.msg_retention
  receive_wait_time_seconds         = var.sqs_config.rcv_time_wait
  kms_master_key_id                 = "alias/aws/sqs"
  kms_data_key_reuse_period_seconds = 300
  redrive_policy = jsonencode({
    deadLetterTargetArn = data.aws_sqs_queue.dead_letter_queue.arn
    maxReceiveCount     = 4
  })

  tags = {
    Environment = "Production"
    Name        = "${var.sqs_config.sqs_version}-${var.group}"
  }
}