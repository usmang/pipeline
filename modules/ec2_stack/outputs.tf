# can put ouput variables here if required any

output "private_subnet_cidr_blocks" {
  value = [for s in data.aws_subnet.private_selected : s.cidr_block]
}

output "public_subnet_cidr_blocks" {
  value = [for s in data.aws_subnet.public_selected : s.cidr_block]
}

output "queue_arn" {
  value = data.aws_sqs_queue.dead_letter_queue.arn
}
