# Creating a random secret with the given parameters
resource "aws_secretsmanager_secret" "sql_server_secret" {
  provider   = aws.dmi-infra
  name       = "provision/${var.db_version}-AWS-${var.secret_env}/${var.db_version}db"
  kms_key_id = data.aws_kms_key.infrastructure_key.arn

  tags = {
    Name = "Random Secret for SQL Server"
  }
}


# Creating a secret version with the secret value
resource "aws_secretsmanager_secret_version" "sql_server_secret_version" {
  provider  = aws.dmi-infra
  secret_id = aws_secretsmanager_secret.sql_server_secret.id
  secret_string = jsonencode({
    username    = var.secret_config.dbuser,
    password    = random_password.sql_password.result,
    engine      = var.secret_config.sql_engine,
    host        = aws_instance.sql_server.private_ip,
    port        = var.secret_config.dbport,
    dbname      = var.secret_config.database_name,
    sa_username = var.secret_config.sa_user,
    sa_password = random_password.sa_password.result
  })
}
# genrating random password to be stored on secret manager
resource "random_password" "sql_password" {
  length           = 16
  special          = true
  override_special = "_%@"
}

resource "random_password" "sa_password" {
  length           = 16
  special          = true
  override_special = "_%@"
}
