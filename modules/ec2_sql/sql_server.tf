resource "aws_instance" "sql_server" {
  provider               = aws.prod01
  instance_type          = var.sql_config.sql_server_instance_type
  vpc_security_group_ids = [aws_security_group.sql_server_sg.id, data.aws_security_group.tcp-embedded.id, aws_security_group.sql_db_access_sg.id]
  ami                    = var.sql_config.sql_server_ami_id
  subnet_id              = values(data.aws_subnet.selected)[var.subnet_index].id
  user_data              = file("${path.module}/user_data.ps1")
  key_name               = var.sql_config.sql_key

  root_block_device {
    volume_type           = var.sql_config.volume_type
    volume_size           = var.sql_config.sql_server_ebs_volume_size
    iops                  = var.sql_config.iops
    delete_on_termination = true
    encrypted             = false
  }

  tags = {
    Name        = "${var.group}-${var.date}-MSSQL-Server"
    Group       = var.group
    Environment = "Production"
    Function    = "MSSQL"
  }
  # IAM Role
  iam_instance_profile = "${var.group}-${var.date}-MSSQL-Server-role"


}

locals {
  sql_server_private_ips = aws_instance.sql_server.*.private_ip
}
