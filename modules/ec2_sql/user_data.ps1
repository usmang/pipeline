param(
[string]$Infrastructure_Account_Region,
[string]$Infrastructure_Account_Number,
[string]$Secret_Name
)

Import-Module SQLPS -DisableNameChecking
Import-Module AWSPowershell

$SA_Username = ""
$SA_Password = ""
$Username = ""
$Password = ""
$DBname   = ""
$IP       = ""
$Port     = ""
$Engine   = ""
$maxMem   = 25000
$maxThreads = 700

$Secret_ARN = "arn:aws:secretsmanager:" + $Infrastructure_Account_Region + ":" + $Infrastructure_Account_Number + ":secret:" + $Secret_Name

$Secret = (Get-SECSecretValue -Region us-east-1 -SecretId $Secret_ARN).SecretString | ConvertFrom-Json

$SA_Username = $Secret.sa_username
$SA_Password = $Secret.sa_password
$Username = $Secret.username
$Password = $Secret.password
$DBname   = $Secret.dbname
$IP       = $Secret.host
$Port     = $Secret.port
$Engine   = $Secret.engine

$instanceName       = $env:COMPUTERNAME

$tcadmin_login_name = $SA_Username
$tcadmin_password   = $SA_Password

$TcSaaS_login_name  = "TcSaaS"
$TcSaaS_password    = $Password

$server             = New-Object -TypeName Microsoft.SqlServer.Management.Smo.Server -ArgumentList $instanceName

$server.Settings.LoginMode = [Microsoft.SqlServer.Management.SMO.ServerLoginMode]::Mixed
$server.Settings.DefaultFile = "D:\DATA\"
$server.Settings.DefaultLog = "E:\LOGS\"
$server.Settings.BackupDirectory = "F:\SaaSAdminToolBackups\"

$server.Configuration.MaxServerMemory.ConfigValue = $maxMem
$server.Configuration.MaxWorkerThreads.ConfigValue = $maxThreads

$server.Alter()

$tcadmin_login = New-Object -TypeName Microsoft.SqlServer.Management.Smo.Login -ArgumentList $server, $tcadmin_login_name
$tcadmin_login.LoginType = [Microsoft.SqlServer.Management.Smo.LoginType]::SqlLogin
$tcadmin_login.PasswordExpirationEnabled = $false
$tcadmin_login.PasswordPolicyEnforced = $false
$tcadmin_login.Create($tcadmin_password)
Write-Host("Login $tcadmin_login_name created successfully.")

$TcSaaS_login = New-Object -TypeName Microsoft.SqlServer.Management.Smo.Login -ArgumentList $server, $TcSaaS_login_name
$TcSaaS_login.LoginType = [Microsoft.SqlServer.Management.Smo.LoginType]::SqlLogin
$TcSaaS_login.PasswordExpirationEnabled = $false
$TcSaaS_login.PasswordPolicyEnforced = $false
$TcSaaS_login.Create($TcSaaS_password)
Write-Host("Login $TcSaaS_login_name created successfully.")

$Roles = $server.Roles

$securityadmin_role = ($Roles | where { ($_.Name -eq "securityadmin")})
$processadmin_role  = ($Roles | where { ($_.Name -eq "processadmin")})
$serveradmin_role   = ($Roles | where { ($_.Name -eq "serveradmin")})
$setupadmin_role    = ($Roles | where { ($_.Name -eq "setupadmin")})
$diskadmin_role     = ($Roles | where { ($_.Name -eq "diskadmin")})
$dbcreator_role     = ($Roles | where { ($_.Name -eq "dbcreator")})
$bulkadmin_role     = ($Roles | where { ($_.Name -eq "bulkadmin")})
$sysadmin_role      = ($Roles | where { ($_.Name -eq "sysadmin")})
$public_role        = ($Roles | where { ($_.Name -eq "public")})

$securityadmin_role.AddMember($tcadmin_login_name)
$processadmin_role.AddMember($tcadmin_login_name)
$serveradmin_role.AddMember($tcadmin_login_name)
$setupadmin_role.AddMember($tcadmin_login_name)
$diskadmin_role.AddMember($tcadmin_login_name)
$dbcreator_role.AddMember($tcadmin_login_name)
$bulkadmin_role.AddMember($tcadmin_login_name)
$sysadmin_role.AddMember($tcadmin_login_name)
$public_role.AddMember($tcadmin_login_name)

$securityadmin_role.AddMember($TcSaaS_login_name)
$processadmin_role.AddMember($TcSaaS_login_name)
$serveradmin_role.AddMember($TcSaaS_login_name)
$setupadmin_role.AddMember($TcSaaS_login_name)
$diskadmin_role.AddMember($TcSaaS_login_name)
$dbcreator_role.AddMember($TcSaaS_login_name)
$bulkadmin_role.AddMember($TcSaaS_login_name)
$sysadmin_role.AddMember($TcSaaS_login_name)
$public_role.AddMember($TcSaaS_login_name)

Restart-Service MSSQLSERVER -Force