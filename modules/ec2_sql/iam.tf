resource "aws_iam_role" "sql_server_role" {
  provider = aws.prod01
  #count               = var.sql_server_count
  name               = "${var.group}-${var.date}-MSSQL-Server-IAM-Role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}

}