
data "aws_vpc" "selected" {
  filter {
    name   = "tag:Environment"
    values = ["${var.environment}"]
  }
}


data "aws_subnets" "selected" {
  filter {
    name   = "tag:Network"
    values = ["${var.Network}"] # insert values here
  }
}

data "aws_subnet" "selected" {
  for_each = toset(data.aws_subnets.selected.ids)
  id       = each.value
}



data "aws_kms_key" "infrastructure_key" {
  provider = aws.dmi-infra
  key_id   = "alias/Infrastructure-Secrets-Key"
}


data "aws_security_group" "tcp-embedded" {
  name = "tcp-embedded-analytics-prod-dmiprod01-sg"
}




