
# Reading the PowerShell script content
data "local_file" "ps1_script" {
  filename = "${path.module}/user_data.ps1"
}

# Executing the PowerShell script using local-exec provisioner
resource "null_resource" "Script" {
  provisioner "local-exec" {
    command     = "powershell -NoProfile -ExecutionPolicy Bypass -File '${path.module}/user_data.ps1' '${var.AWS_REGION}' '${var.secret_account}' '${aws_secretsmanager_secret.sql_server_secret.tags.Name}'"
    interpreter = ["powershell"]
  }
}

