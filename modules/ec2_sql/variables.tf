
variable "Network" {
  type = string
}
variable "date" {
  type = string
}

variable "secret_config" {
  type = object({
    admin_user    = string
    sql_enginev   = string
    dbport        = number
    database_name = string
    dbuser        = string
    sa_user       = string
    db_version    = string
  })
}

variable "sql_config" {
  type = object({
    iops                       = number
    sql_server_ebs_volume_size = number
    sql_server_ami_id          = string
    volume_type                = string
    sql_key                    = string
    ebs_az                     = string
    sql_server_instance_type   = string
  })
}

variable "tags" {
  type = object({
    environment = string
    owner       = string
    AWS_REGION  = string
    ticket      = string
    group       = string
  })
}


variable "sql_server_ingress_rules" {
  type = map(object({
    type        = string
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = optional(list(string))
    source      = optional(string)
    description = string
  }))
}

variable "sql_server_egress_rules" {
  type = map(object({
    type        = string
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = optional(list(string))
  }))
  default = {
    rule1 = {
      type        = "egress"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
}
