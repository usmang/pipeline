# Security group for SQL Server
resource "aws_security_group" "sql_server_sg" {
  provider    = aws.prod01
  name        = "${var.group}-${var.date}-MSSQL-Server-SG"
  description = "Security Group for SQL Server"
  vpc_id      = data.aws_vpc.selected.id
  tags = {
    Name = "${var.group}-${var.date}-MSSQL-Server-SG"
  }
}

# Security group rules for SQL Server SG
resource "aws_security_group_rule" "sql_server_sg_ingress" {
  provider = aws.prod01
  for_each = var.sql_server_ingress_rules

  type                     = each.value.type
  from_port                = each.value.from_port
  to_port                  = each.value.to_port
  security_group_id        = aws_security_group.sql_server_sg.id
  protocol                 = each.value.protocol
  cidr_blocks              = each.value.cidr_blocks
  source_security_group_id = each.value.source != null ? each.value.source : null
  description              = each.value.description
}

resource "aws_security_group_rule" "sql_server_sg_egress" {
  provider = aws.prod01
  for_each = var.sql_server_egress_rules

  type              = each.value.type
  from_port         = each.value.from_port
  to_port           = each.value.to_port
  security_group_id = aws_security_group.sql_server_sg.id
  protocol          = each.value.protocol
  cidr_blocks       = each.value.cidr_blocks
}

# Security group for SQL Server
resource "aws_security_group" "sql_db_access_sg" {
  provider    = aws.prod01
  name        = "${var.group}-${var.date}-MSSQL-Server-DB-Access-SG"
  description = "Security Group for SQL Server"
  vpc_id      = data.aws_vpc.selected.id
  tags = {
    Name = "${var.group}-${var.date}-MSSQL-Server-DB-Access-SG"
  }
}

# Security group rules for SQL Server SG
resource "aws_security_group_rule" "sql_db_access_sg_ingress" {
  provider = aws.prod01
  for_each = var.sql_db_access_sg_ingress

  type                     = each.value.type
  from_port                = each.value.from_port
  to_port                  = each.value.to_port
  security_group_id        = aws_security_group.sql_db_access_sg.id
  protocol                 = each.value.protocol
  cidr_blocks              = each.value.cidr_blocks
  source_security_group_id = each.value.source != null ? each.value.source : null
  description              = each.value.description
}

resource "aws_security_group_rule" "sql_db_access_sg_egress" {
  provider = aws.prod01
  for_each = var.sql_db_access_sg_egress

  type              = each.value.type
  from_port         = each.value.from_port
  to_port           = each.value.to_port
  security_group_id = aws_security_group.sql_db_access_sg.id
  protocol          = each.value.protocol
  cidr_blocks       = each.value.cidr_blocks
}


