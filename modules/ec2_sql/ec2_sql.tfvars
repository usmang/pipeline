# terraform.tfvars
group                      = "Prod39"
AWS_REGION                 = "us-east-1"
sql_server_instance_type   = "r5.large"
sql_server_ebs_volume_size = 200
sql_server_ami_id          = "ami-0e0ba03fa378e9e44" # MSSQL server standard AMI
secret_env                 = "Prod39"
multi_az                   = true
db_version                 = "v7"
bucket_arn                 = "arn:aws:s3:::sqlbackups-766455805380"
account_id_iam             = 766455805380
backup_retention_period    = 7
ticket                     = "DEVOPS-11491"
max_allocated_storage      = 1000
engine_version             = "14.00.3451.2.v1"

rds_instance_class    = "db.m6i.2xlarge"
rds_allocated_storage = 100
rds_username          = "tcadmin"


backup_window      = "05:00-06:00"
maintenance_window = "Sun:00:00-Sun:01:00"

sql_server_ingress_rules = {
  rule1 = {
    type      = "ingress"
    from_port = 1433
    to_port   = 1433
    protocol  = "tcp"
    # CIDR BLOCK in Ingress rules are the IPS from different services needed to be allowed 
    cidr_blocks = ["10.45.0.0/16"]
    description = "ingress rule"
  }
  rule2 = {
    type        = "ingress"
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    source      = "sg-0d4f1e0590f157454"
    description = "saas_admin_server_sg"
  }
  rule3 = {
    type        = "ingress"
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    source      = "sg-09f5f8fdb71d1b36e"
    description = "ophelia-infrastructure"
  }
  rule4 = {
    type        = "ingress"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    source      = "sg-0ddc27bb3ab6d2aa8"
    description = "scale_ft_acess"
  }
  rule5 = {
    type        = "ingress"
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    source      = "sg-0297eda74f57a4933"
    description = "database_managment_sg"
  }

  rule6 = {
    type        = "ingress"
    from_port   = 10050
    to_port     = 10051
    protocol    = "tcp"
    source      = "sg-03915fb7a17f70955"
    description = "VPC_Perring"
  }

  rule7 = {
    type        = "ingress"
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    source      = "sg-0e4df5566b9d1c67e"
    description = " Rundeck_security_group"
  }
  rule8 = {
    type        = "ingress"
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    source      = "sg-0b21d69a8d14e21bf"
    description = " API_ACCESS"
  }

}



app_lb_ingress_rules = {
  rule1 = {
    type      = "ingress"
    from_port = 443
    to_port   = 443
    protocol  = "tcp"
    # CIDR BLOCK in Ingress rules are the IPS from different services needed to be allowed 
    cidr_blocks      = ["198.41.128.0/17", "103.28.248.0/22", "188.114.96.0/20", "173.245.48.0/20", "172.64.0.0/13", "45.223.0.0/16", "197.234.240.0/22", "104.16.0.0/13", "108.162.192.0/18", "131.0.72.0/22", "192.230.64.0/18", "149.126.72.0/21", "185.11.124.0/22", "103.31.4.0/22", "103.22.200.0/22", "45.60.0.0/16", "104.24.0.0/14", "198.143.32.0/19", "103.21.244.0/22", "141.101.64.0/18", "162.158.0.0/15", "190.93.240.0/20", "107.154.0.0/16", "45.64.64.0/22", "199.83.128.0/21"]
    ipv6_cidr_blocks = ["2405:8100::/32", "2a02:e980::/29", "2c0f:f248::/32", "2803:f800::/32", "2405:b500::/32", "2a06:98c0::/29", "2606:4700::/32", "2400:cb00::/32"]
  }
}

app_lb_egress_rules = {
  rule1 = {
    type             = "egress"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = []
  }
}

app_server_target_port = 7011
app_server_protocol    = "HTTP"

listener_port     = 443
listener_protocol = "HTTPS"
ssl_policy        = "ELBSecurityPolicy-TLS13-1-2-2021-06"
lb_certificate    = "arn:aws:acm:us-east-1:580423500457:certificate/c03a892d-8d25-4844-aad4-feccc6aafe20"

health_check_timeout  = 5
unhealthy_threshold   = 2
healthy_threshold     = 2
health_check_protocol = "HTTP"
health_check_path     = "/api/v0000/employeeSessions/0/ping"
health_interval       = 30

delay_seconds = 0
max_msg_size  = 256000
msg_retention = 345600
rcv_time_wait = 2
account_name  = "dmi-infrastruture01"
environment   = "Production"
owner         = "usman khaliq"

rds_security_group_ingress = {
  rule1 = {
    type      = "ingress"
    from_port = 1433
    to_port   = 1433
    protocol  = "tcp"
    # CIDR BLOCK in Ingress rules are the IPS from different services needed to be allowed 
    cidr_blocks = ["10.40.0.0/16"]
    description = "tcp-global-api-prod"
  }
  rule2 = {
    type        = "ingress"
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    source      = "sg-0d4f1e0590f157454"
    description = "1433 from SaaS Admin Server"
  }
  rule3 = {
    type        = "ingress"
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    source      = "sg-09f5f8fdb71d1b36e"
    description = "Ophelia-Infrastructure"
  }
  rule4 = {
    type        = "ingress"
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    source      = "sg-0297eda74f57a4933"
    description = "1433 from DB Management Server"
  }
  rule5 = {
    type        = "ingress"
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    source      = "sg-0e3bb0c367889cb3e"
    description = "Prod36 DB Access to RDS rule"
  }
}

rds_security_group_egress = {
  rule1 = {
    type             = "egress"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}


# Key pair name for SQL Server
sql_server_keypair_name = "sql_server_key"




# Egress rules for SQL Server
sql_server_egress_rules = {
  rule1 = {
    type             = "egress"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = []
  }
}



# Number of SQL Server instances to create

# Date value
date = "20230622"

# Secret environment value


# List of existing policies
existing_policies = [
  "arn:aws:iam::766455805380:policy/Custom-Policy-1",
  "arn:aws:iam::766455805380:policy/Prod20-Secrets-Access",
  "arn:aws:iam::aws:policy/AmazonSQSFullAccess",
  "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM"
]

# Database port number
dbport = 1433

# Project name
project_name = "v7-AWS"

# Index of the subnet to use
subnet_index = 0

# Database name
database_name = "master"

# Database user
dbuser = "TcSaaS"

# Volume type
volume_type = "gp3"

# IOPS value
iops = 3000

# SA user
sa_user = "tcadmin"

# Environment type
ENVIRONMENT = "Production"

# Network type
Network = "Private"


