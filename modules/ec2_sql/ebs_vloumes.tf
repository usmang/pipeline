
resource "aws_ebs_volume" "Logs-Drive" {
  availability_zone = var.sql_config.ebs_az
  size              = var.sql_config.sql_server_ebs_volume_size
  type              = var.sql_config.volume_type
  encrypted         = true
  tags = {
    Name = "${var.group}-${var.date}-MSSQL-Server-Logs-Drive"
  }
}

resource "aws_ebs_volume" "Backup-Drive" {
  availability_zone = var.sql_config.ebs_az
  size              = var.sql_config.sql_server_ebs_volume_size
  type              = var.sql_config.volume_type
  encrypted         = true
  tags = {
    Name = "${var.group}-${var.date}-MSSQL-Server-Backups-Drive"
  }
}
resource "aws_ebs_volume" "Data-Drive" {
  availability_zone = var.sql_config.ebs_az
  size              = var.sql_config.sql_server_ebs_volume_size
  type              = var.sql_config.volume_type
  encrypted         = true

  tags = {
    Name = "${var.group}-${var.date}-MSSQL-Server-Data-Drive"
  }
}

resource "aws_volume_attachment" "ebs_att_Logs" {
  device_name = "xvdf"
  volume_id   = aws_ebs_volume.Logs-Drive.id
  instance_id = aws_instance.sql_server.id
}

resource "aws_volume_attachment" "ebs_att_Backup" {
  device_name = "xvdg"
  volume_id   = aws_ebs_volume.Backup-Drive.id
  instance_id = aws_instance.sql_server.id
}

resource "aws_volume_attachment" "ebs_att_Data" {
  device_name = "xvdh"
  volume_id   = aws_ebs_volume.Data-Drive.id
  instance_id = aws_instance.sql_server.id
}

