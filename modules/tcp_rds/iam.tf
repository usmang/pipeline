# Define the IAM policy
resource "aws_iam_policy" "rds_policy" {
  provider    = aws.prod01
  name        = "${var.group}-${var.rds_config.rds_version}-policy"
  description = "Policy for RDS DB option group"

 
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action   = ["kms:Decrypt", "kms:DescribeKey", "kms:GenerateDataKey"]
        Effect   = "Allow"
        Resource = [aws_kms_key.rds_key.arn]
      },
      {
        Action   = ["s3:ListBucket", "s3:GetBucketLocation"]
        Effect   = "Allow"
        Resource = ["arn:aws:s3:::s"]
      },
      {
        Action   = ["s3:GetObjectMetaData", "s3:GetObject", "s3:PutObject", "s3:ListMultipartUploadParts", "s3:AbortMultipartUpload"]
        Effect   = "Allow"
        Resource = ["arn:aws:s3:::*"]
      },

    ]
  })
}

#  IAM role and attach the policy
resource "aws_iam_role" "rds_role" {
  provider = aws.prod01
  name     = "${var.group}-${var.rds_config.rds_version}-IAM-Role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "rds.amazonaws.com"
        }
      }
    ]
  })
}

# Attach the policy to the IAM role
resource "aws_iam_role_policy_attachment" "policy_attachment" {
  provider   = aws.prod01
  policy_arn = aws_iam_policy.rds_policy.arn
  role       = aws_iam_role.rds_role.name
}