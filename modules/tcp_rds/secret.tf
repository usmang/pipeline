# Creating a random secret with the given parameters
resource "aws_secretsmanager_secret" "sql_server_secret" {
  provider = aws.dmi-infra
  name     = "provision/${var.secret_config.db_version}-AWS-${var.secret_config.secret_env}/${var.secret_config.db_version}db"

  tags = {
    Name = "Random Secret for SQL Server"
  }
}

# Creating a secret version with the secret value
resource "aws_secretsmanager_secret_version" "sql_server_secret_version" {
  provider  = aws.dmi-infra
  secret_id = aws_secretsmanager_secret.sql_server_secret.id
  secret_string = jsonencode({
    username = var.secret_config.admin_user,
    password = random_password.sql_password.result,
    engine   = var.secret_config.sql_engine,
    host     = aws_db_instance.rds_instance.endpoint,
    port     = var.secret_config.dbport,
    dbname   = var.secret_config.database_name
  })
}

# genrating random password to be stored on secret manager
resource "random_password" "sql_password" {
  length           = 16
  special          = true
  override_special = "_%#"
}
