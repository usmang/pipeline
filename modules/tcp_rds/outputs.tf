output "subnet_cidr_blocks" {
  value = [for s in data.aws_subnet.selected : s.cidr_block]
}


output "subnet_ids" {
  value = keys(data.aws_subnet.selected)
}

output "security_group_id" {
  value = data.aws_security_group.tcp-embedded.id
}

output "security_group_db_acess_id" {
  value = aws_security_group.rds_sg_db_acess.id # RDS Access seurity group
}

output "security_group_db_sg_id" {
  value = aws_security_group.rds_security_group.id
}