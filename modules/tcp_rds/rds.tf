# Create RDS instance
resource "aws_db_instance" "rds_instance" {
  provider                   = aws.prod01
  identifier                 = "${lower(var.group)}-${var.rds_config.rds_version}"
  engine                     = var.rds_config.rds_engine
  engine_version             = var.rds_config.engine_version
  instance_class             = var.rds_config.rds_instance_class
  allocated_storage          = var.rds_config.rds_allocated_storage
  max_allocated_storage      = var.rds_config.max_allocated_storage
  multi_az                   = var.rds_config.multi_az
  username                   = var.rds_config.rds_username
  password                   = random_password.sql_password.result
  db_subnet_group_name       = aws_db_subnet_group.db_subnet.name
  publicly_accessible        = false # Set to true if you want the RDS instance to be publicly accessible
  parameter_group_name       = aws_db_parameter_group.db_parameter.name
  option_group_name          = aws_db_option_group.rds_option_group.name
  kms_key_id                 = aws_kms_key.rds_key.arn
  storage_encrypted          = true
  license_model              = "license-included"
  final_snapshot_identifier  = "${var.group}final-snapshot"
  skip_final_snapshot        = false
  vpc_security_group_ids     = [aws_security_group.rds_security_group.id, data.aws_security_group.tcp-embedded.id, aws_security_group.rds_sg_db_acess.id]
  auto_minor_version_upgrade = true
  backup_retention_period    = var.rds_config.backup_retention_period
  backup_window              = var.rds_config.backup_window
  maintenance_window         = var.rds_config.maintenance_window
  deletion_protection        = true
  timeouts {
    create = "2h"
    delete = "2h"
    update = "2h"
  }
}

resource "aws_db_subnet_group" "db_subnet" {
  provider   = aws.prod01
  name       = "${lower(var.group)}-${var.rds_config.rds_version}-subnet-group"
  subnet_ids = values(data.aws_subnet.selected)[*].id
}

# resource KMS key
resource "aws_kms_key" "rds_key" {
  provider    = aws.prod01
  description = "KMS key for RDS encryption"
  policy      = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Enable IAM User Permissions",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::${var.account_id_iam}:root"
      },
      "Action": [
        "kms:*"
      ],
      "Resource": "*"
    }
  ]
}
EOF

  tags = {
    Name = "${var.group}-${var.rds_config.rds_version}-KMS-KEY"
  }
}

# creating Alias for KMS key
resource "aws_kms_alias" "rds_key_alias" {
  provider      = aws.prod01
  name          = "alias/${var.group}-${var.rds_config.rds_version}-KMS-KEY"
  target_key_id = aws_kms_key.rds_key.key_id
}

# DB parameter group
resource "aws_db_parameter_group" "db_parameter" {
  provider = aws.prod01
  name     = "${lower(var.group)}-parameters-sql15"
  family   = var.rds_config.sql_server_version
}



#DB option group
resource "aws_db_option_group" "rds_option_group" {
  provider                 = aws.prod01
  name                     = "${lower(var.group)}-${var.rds_config.rds_version}-option-group"
  option_group_description = "Terraform Option Group for rds  cluster"
  engine_name              = var.rds_config.rds_engine
  major_engine_version     = "15.00"

  option {
    option_name = "SQLSERVER_BACKUP_RESTORE"

    option_settings {
      name  = "IAM_ROLE_ARN"
      value = aws_iam_role.rds_role.arn
    }
  }
}