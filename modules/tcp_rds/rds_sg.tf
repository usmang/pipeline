# Create RDS security group
resource "aws_security_group" "rds_security_group" {
  provider    = aws.prod01
  name        = "${var.group}-RDS-1-SG"
  description = "Security Group for RDS instance"
  vpc_id      = data.aws_vpc.selected.id
  tags = {
    Name = "${var.group}-RDS-1-SG"
  }
}


resource "aws_security_group_rule" "rds_security_group_ingress" {
  provider = aws.prod01
  for_each = var.rds_security_group_ingress

  type                     = each.value.type
  from_port                = each.value.from_port
  to_port                  = each.value.to_port
  security_group_id        = aws_security_group.rds_security_group.id
  protocol                 = each.value.protocol
  cidr_blocks              = each.value.cidr_blocks
  source_security_group_id = each.value.source != null ? each.value.source : null
  description              = each.value.description
}

resource "aws_security_group_rule" "rds_security_group_egress" {
  provider = aws.prod01
  for_each = var.rds_security_group_egress

  type              = each.value.type
  from_port         = each.value.from_port
  to_port           = each.value.to_port
  security_group_id = aws_security_group.rds_security_group.id
  protocol          = each.value.protocol
  cidr_blocks       = each.value.cidr_blocks
}

#-------------------------------------------------------------------------------------------

# Create RDS security group
resource "aws_security_group" "rds_sg_db_acess" {
  provider    = aws.prod01
  name        = "${var.group}-RDS-1-DB-Access-SG"
  description = "Security Group for RDS instance"
  vpc_id      = data.aws_vpc.selected.id
  tags = {
    Name = "${var.group}-RDS-1-DB-Access-SG"
  }
}

resource "aws_security_group_rule" "rds_sg_db_ingress" {
  provider = aws.prod01
  for_each = var.rds_sg_db_ingress

  type                     = each.value.type
  from_port                = each.value.from_port
  to_port                  = each.value.to_port
  security_group_id        = aws_security_group.rds_sg_db_acess.id
  protocol                 = each.value.protocol
  cidr_blocks              = each.value.cidr_blocks
  source_security_group_id = each.value.source != null ? each.value.source : null
  description              = each.value.description
}

resource "aws_security_group_rule" "rds_sg_db_egress" {
  provider = aws.prod01
  for_each = var.rds_sg_db_egress

  type              = each.value.type
  from_port         = each.value.from_port
  to_port           = each.value.to_port
  security_group_id = aws_security_group.rds_sg_db_acess.id
  protocol          = each.value.protocol
  cidr_blocks       = each.value.cidr_blocks
}
