variable "tags" {
  type = object({
    environment = string
    owner       = string
    AWS_REGION  = string
    ticket      = string
  })
}

variable "group" {
  type= string
}

variable "secret_config" {
  type = object({
    admin_user    = string
    dbport        = number
    database_name = string
    dbuser        = string
    sa_user       = string
    db_version    = string
    secret_env    = string
    sql_engine   = string
  })
}

variable "rds_config" {
  type = object({
    rds_instance_class      = string
    rds_allocated_storage   = number
    rds_username            = string #used in rds indentifer like rds or rds-1
    rds_major_version       = string
    backup_retention_period = number
    backup_window           = string
    maintenance_window      = string
    multi_az                = bool
    rds_engine              = string
    engine_version          = string
    rds_version             = string
    sql_server_version      = string
    max_allocated_storage   = number
  })
}


variable "Network" {
  type = string
}


variable "rds_security_group_ingress" {
  type = map(object({
    type        = string
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = optional(list(string))
    source      = optional(string)
    description = string
  }))
}

variable "rds_security_group_egress" {
  type = map(object({
    type        = string
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = optional(list(string))
  }))
}


variable "rds_sg_db_ingress" {
  type = map(object({
    type        = string
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = optional(list(string))
    source      = optional(string)
    description = string
  }))
}

variable "rds_sg_db_egress" {
  type = map(object({
    type        = string
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = optional(list(string))
  }))
}

variable "account_id_iam" {
  type = number
}

variable "bucket_arn" {
  type = string
}