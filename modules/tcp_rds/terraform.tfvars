secret_env         = "test39" # secret_env is a seprate variable for secret amanger secret name to incorporate 
rds_engine         = "sqlserver-se"

# Add appropriate values for rds_instance_class, rds_allocated_storage, rds_username, and rds_password
rds_instance_class    = "db.t2.micro"
rds_allocated_storage = 20


db_version    = "v7"
database_name = "master"
dbport        = 1433
# Add appropriate value for account_id_iam if needed
ENVIRONMENT = "Production"
rds_version = "rds-1"
# Add appropriate value for bucket_arn if needed
multi_az                = true
backup_retention_period = 7

backup_window      = "05:00-06:00"
maintenance_window = "Mon:00:00-Mon:01:00"
Network            = "Private"
