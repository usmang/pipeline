
data "aws_vpc" "selected" {
  filter {
    name   = "tag:Environment"
    values = ["${var.tags.environment}"]
  }
}


data "aws_subnets" "selected" {
  filter {
    name   = "tag:Network"
    values = ["Private"]
  }
}

data "aws_subnet" "selected" {
  for_each = toset(data.aws_subnets.selected.ids)
  id       = each.value
}


data "aws_security_group" "tcp-embedded" {
  name = "tcp-embedded-analytics-tcp-qa-sg"
}

