 module "ec2_stack" {
  source = "./modules/ec2_stack"
  providers = {
    aws.dmi-infra = aws.dmi-infra
    aws.prod01    = aws.prod01
  }
  tags                    = var.tags
  sqs_config              = var.sqs_config
  lb_config               = var.lb_config
  tg_config               = var.tg_config
  group                   = var.group
  app_lb_ingress_rules    = var.app_lb_ingress_rules
  app_lb_egress_rules     = var.app_lb_egress_rules
 }

module "tcp_rds" {
  source = "./modules/tcp_rds"
  providers = {
    aws.dmi-infra = aws.dmi-infra
    aws.prod01    = aws.prod01
  }
  account_id_iam             = var.account_id_iam
  bucket_arn                 = var.bucket_arn
  Network                    = var.Network
  rds_config                 = var.rds_config
  tags                       = var.tags
  secret_config              = var.secret_config
  rds_sg_db_ingress          = var.rds_sg_db_ingress
  rds_sg_db_egress           = var.rds_sg_db_egress
  rds_security_group_ingress = var.rds_security_group_ingress
  rds_security_group_egress  = var.rds_security_group_egress
  group                      = var.group
  
}

