terraform {
  backend "s3" {
    bucket               = "s3-backend-path"
    key                  = "example"
    workspace_key_prefix = "workspaces"
    region               = "us-east-1"
    profile              = "xyz"
  }
}

