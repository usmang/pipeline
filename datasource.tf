
data "aws_vpc" "selected" {
  filter {
    name   = "tag:Environment"
    values = ["${var.tags.environment}"]
  }
}


data "aws_subnets" "selected" {
  filter {
    name   = "tag:Network"
    values = ["${var.Network}"] # insert values here
  }
}

data "aws_subnet" "selected" {
  for_each = toset(data.aws_subnets.selected.ids)
  id       = each.value
}

output "subnet_cidr_blocks" {
  value = [for s in data.aws_subnet.selected : s.cidr_block]
}


data "aws_kms_key" "infrastructure_key" {
  provider = aws.dmi-infra
  key_id   = "alias/Infrastructure-Secrets-Key"
}



